<?php

/**
 * Loads country object.
 *
 * @param int $id
 *   country id
 *
 * @return object
 *   country object
 */
function uc_optile_load_country($id) {
  return db_query("SELECT * FROM {uc_countries} WHERE country_id = :id", array(':id' => $id))->fetchAssoc();
}

/**
 * Retrieves a zone using its ID.
 *
 * @param $id
 *   The zone's ID.
 */
function uc_optile_load_zone($id) {
  return db_query("SELECT * FROM {uc_zones} WHERE zone_id = :id", array(':id' => $id))->fetchAssoc();
}

/**
 * Gets the Optile configuration settings into array.
 *
 * - merchant code
 * - token
 * - url
 *
 * @return array
 *   array of optile settings
 */
function uc_optile_get_keys() {
  $merchant_code = variable_get('uc_optile_merchant_code');
  $merchant_token = variable_get('uc_optile_merchant_token');
  $api_url = variable_get('uc_optile_api_url');

  return array($merchant_code, $merchant_token, $api_url);
}

function uc_optile_new_list_request($form, &$form_state, $order) {
  $billing_country = uc_optile_load_country($order->billing_country);

  list($merchantCode, $merchantToken, $apiUrl) = uc_optile_get_keys();

  $request = \Optile\Request\RequestFactory::getListRequest($apiUrl);
  $request->setMerchantCode($merchantCode);
  $request->setMerchantToken($merchantToken);
  $request->setTransactionId($order->order_id);
  $request->setCountry($billing_country['country_iso_code_2']);

  $request->addPayment()
          ->setAmount($order->order_total)
          ->setCurrency($order->currency)
          ->setReference("Order #{$order->order_id}, ". uc_store_name()." order");

  array_walk($order->products, function ($data, $id) use ($order, $request) {
    $request->addProduct()
            ->setCode($data->model)
//            ->setName($data->title . ' x' . $data->qty)
            ->setName($data->title)
            ->setQuantity($data->qty)
            ->setAmount($data->price * $data->qty);
  });

  // adds shipping as a separate product
  foreach($order->line_items as $id => $line_item) {
    if($line_item['type'] == 'shipping') {
      $request->addProduct()
              ->setCode($line_item['line_item_id'])
              ->setName($line_item['title'])
              ->setQuantity(1)
              ->setAmount($line_item['amount']);
    }
  }

  $email = $order->primary_email != "" ? $order->primary_email : "";
  $uid = $order->uid != 0 ? $order->uid : "None";

  $reqCustomer = $request->addCustomer();

  $reqCustomer->setEmail($email);
  $reqCustomer->setNumber($uid);
  $reqCustomer->addName()
            ->setFirstName($order->billing_first_name)
            ->setLastName($order->billing_last_name);


  if(isset($order->billing_first_name) && strlen($order->billing_first_name) > 0) {
    $reqCustomer->addAddress(\Optile\Request\CustomerAddress::TYPE_BILLING)
            ->setCity($order->billing_city)
            ->setCountry($billing_country['country_iso_code_2'])
            ->setState(uc_get_zone_code($order->billing_zone))
            ->setStreet($order->billing_street1 . " " . $order->billing_street2)
            ->setZip($order->billing_postal_code)
            ->addName()
              ->setFirstName($order->billing_first_name)
              ->setLastName($order->billing_last_name);
  }

  if(isset($order->delivery_first_name) && strlen($order->delivery_first_name) > 0) {
    $delivery_country = uc_optile_load_country($order->delivery_country);

    $reqCustomer->addAddress(\Optile\Request\CustomerAddress::TYPE_SHIPPING)
        ->setCity($order->delivery_city)
        ->setCountry($delivery_country['country_iso_code_2'])
        ->setState(uc_get_zone_code($order->delivery_zone))
        ->setStreet($order->delivery_street1 . " " . $order->delivery_street2)
        ->setZip($order->delivery_postal_code)
        ->addName()
          ->setFirstName($order->delivery_first_name)
          ->setLastName($order->delivery_last_name);
}

  $request->addCallback()
          ->setReturnUrl(url('uc_optile/return', array('absolute' => TRUE)))
          ->setCancelUrl(url('uc_optile/cancel', array('absolute' => TRUE)))
          ->setNotificationUrl(url('uc_optile/ipn', array('absolute' => TRUE)));

  return $request;
}

function uc_optile_find_optile_order_data($order_id) {
  $result = db_query("SELECT * FROM {uc_optile_order_data} WHERE order_id = :id", array(":id" => $order_id))->fetchAssoc();

  return $result;
}

function uc_optile_create_optile_order_data($order_id) {
  $result = uc_optile_find_optile_order_data($order_id);
  if(!$result) {
    db_insert('uc_optile_order_data')
    ->fields(array(
      'order_id' => $order_id
    ))
    ->execute();
  }
}
