<?php

/**
 * AJAX checkout form is broken when you go 'back' from PayPal.
 *
 * Adds the header so that 'back' button must reload the page.
 */
function uc_optile_uc_cart_checkout_start() {
   drupal_add_http_header('Cache-Control', 'no-cache, no-store, must-revalidate, post-check=0, pre-check=0');
   drupal_add_http_header('Pragma', 'no-cache');
   drupal_add_http_header('Expires', '0');

   // adds payment pane reload triggers to all the fields that are included
   // in the LIST request
   $config = variable_get('uc_ajax_checkout', uc_optile_uc_ajax_defaults('checkout'));
   $add_ajax_payment_pane_reload_trigger = uc_optile_watched_fields();
   $payment_pane = drupal_map_assoc(array('payment-pane'));

   foreach($add_ajax_payment_pane_reload_trigger as $v) {
     if(!isset($config[$v])) $config[$v] = array();

     $config[$v] = array_merge($config[$v], $payment_pane);
   }

   variable_set('uc_ajax_checkout', $config);
}

/**
 * Implements hook_uc_payment_method_checkout_alter().
 */
function uc_optile_uc_payment_method_checkout_alter(&$methods, $order) {
  if (isset($methods['optile'])) {
    $merchant_code = variable_get('uc_optile_merchant_code', '');
    $merchant_token = variable_get('uc_optile_merchant_token', '');

    if($merchant_code == '' || $merchant_token == '') {
      unset($methods['optile']);
    }
  }
}


/**
 * Retrieve the default ajax behaviors for a target form.
 *
 * @param $target_form
 *   The form whose default behaviors are to be retrieved.
 *
 * @return
 *   The array of default behaviors for the form.
 */
function uc_optile_uc_ajax_defaults($target_form) {
  switch ($target_form) {
    case 'checkout':
      $quotes_defaults = drupal_map_assoc(array('payment-pane', 'quotes-pane'));
      return array(
        'panes][delivery][address][delivery_country' => $quotes_defaults,
        'panes][delivery][address][delivery_postal_code' => $quotes_defaults,
        'panes][delivery][select_address' => $quotes_defaults,
        'panes][billing][address][billing_country' => array('payment-pane' => 'payment-pane'),
      );
    default:
      return array();
  }
}

function uc_optile_watched_fields() {
  return array(
          'panes][customer][primary_email',
          'panes][delivery][select_address',
          'panes][delivery][address][delivery_first_name',
          'panes][delivery][address][delivery_last_name',
          'panes][delivery][address][delivery_street1',
          'panes][delivery][address][delivery_city',
          'panes][delivery][address][delivery_zone',
          'panes][delivery][address][delivery_country',
          'panes][delivery][address][delivery_postal_code',
          'panes][billing][copy_address',
          'panes][billing][select_address',
          'panes][billing][address][billing_first_name',
          'panes][billing][address][billing_last_name',
          'panes][billing][address][billing_street1',
          'panes][billing][address][billing_city',
          'panes][billing][address][billing_zone',
          'panes][billing][address][billing_country',
          'panes][billing][address][billing_postal_code',
          'panes][quotes][quote_button',
        );
}

/**
 * Overriding checkout form for Optile payment method.
 *
 * @param array $form
 *   drupal form array
 * @param array $form_state
 *   drupal form state
 * @param object $order
 *   drupal order object
 *
 * @return array
 *   form to render
 */
function uc_optile_cart_details_form($form, &$form_state, $order) {

  // T: Unsetting double order review.
  unset($form['panes']);

  $order->payment_details = array('');

  if(isset($form_state['values']['panes']['payment']['details']['optile-reload-list-request']) && $form_state['values']['panes']['payment']['details']['optile-reload-list-request'] == '1') {
    $request = \Optile\Request\RequestFactory::getReloadListRequest($form_state['values']['panes']['payment']['details']['optile-list-request-self']);
    $form_state['input']['panes']['payment']['details']['optile-reload-list-request'] = '0';
  } else {
    $request = uc_optile_new_list_request($form, $form_state, $order);
  }

  $result = $request->send();

  $networks = $result->getNetworks();
  $networks_formatted = array();

  foreach($networks as $groupName => $group) {
    foreach ($group as $network) {
      $networks_formatted[$network->getCode()] = '<img src="' . $network->getLinks()->getLogoLink() . '" />';
    }
  }

  $form["optile-selected-network"] = array(
    '#type' => 'radios',
    '#title' => t("Select payment network"),
    '#options' => $networks_formatted,
    // N: preselects first network.
    '#default_value' => count($networks_formatted) > 0 ? key($networks_formatted) : NULL,
  );

  $optile_forms_js = "window.optileForms = [];";

  foreach($networks as $groupName => $group) {
    foreach ($group as $network) {
      $form['optile-selected-network'][$network->getCode()]['#description'] = str_ireplace('${formId}', $network->getCode(), '<div class="optile-network" id="optile-' . $network->getCode() . '-network" >' . $network->getLinks()->getLocalizedFormHtml() . '</div>');
      $optile_forms_js .= <<<JS
            window.optileForms.push(new OptilePaymentNetworkForm('{$network->getCode()}', '{$network->getLinks()->getValidationLink()}', '{$network->getLinks()->getOperation()}'));
JS;
    }
  }
  $form['uc_optile_executor'] = array(
    '#markup' => <<<JS
          <script type="text/javascript">
          {$optile_forms_js}
          </script>
JS
  );

  $listRequestLinks = $result->getLinks();
  $form['optile-list-request-self'] = array('#type' => 'hidden', '#value' => $listRequestLinks['self']);
  $form['optile-reload-list-request'] = array('#type' => 'hidden', '#default_value' => '0');
  $form['optile-proceed'] = array('#type' => 'hidden', '#default_value' => '0');
  $form['optile-just-want-validation'] = array('#type' => 'hidden', '#default_value' => '0');

  return $form;
}

/**
 * Callback function to perform various operations for a payment method.
 *
 * Possible operations are as follows:
 * - "cart-details": The payment method has been selected at checkout. Return
 *   a form or render array to be displayed in the payment method pane.
 * - "cart-process": Called when the user submits the checkout form with this
 *   payment method selected, used to process any form elements output by the
 *   'cart-details' op. Return FALSE to abort the checkout process, or NULL or
 *   TRUE to continue with checkout.
 * - "cart-review": Called when the checkout review page is being displayed.
 *   Return an array of data to be displayed below the payment method title on
 *   the checkout review page.
 * - "customer-view": Called when the order is being displayed to a customer.
 *   Return a render array to be displayed to customers.
 * - "order-delete": Called when an order is being deleted. Payment methods
 *   should clean up any extra data they stored related to the order.
 * - "order-details": Called when an order is being edited by an administrator.
 *   Return a string or a form array to be displayed to the administator.
 * - "order-load": Called from hook_uc_order('load') when this payment method
 *   is selected for the order.
 * - "order-process": Called when an order has been edited by an administrator.
 *   Process any form elements returned by the "order-details" op.
 * - "order-save": Called from hook_uc_order('save') when this payment method
 *   is selected for the order.
 * - "order-submit": Called from hook_uc_order('submit') when this payment
 *   method is selected for the order.
 * - "order-view": Called when the order is being displayed on the order admin
 *   pages. Return a render array to be displayed to administrators.
 * - "settings": Called when the payment methods page is being displayed.
 *   Return a system settings form array to configure the payment method.
 *
 * @see hook_uc_payment_method()
 *
 * @param string $op
 *   The operation being performed.
 * @param object $order
 *   The order object that relates to this operation.
 * @param array $form
 *   Where applicable, the form object that relates to this operation.
 * @param array $form_state
 *   Where applicable, the form state that relates to this operation.
 *
 * @return array
 *   Dependent on $op.
 */
function uc_optile_method_callback($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case "settings":
      $form = uc_optile_settings_form($form, $form_state);
      break;

    case "cart-details":
      $form = uc_optile_cart_details_form($form, $form_state, $order);
      break;

    case "cart-process":
      $order->payment_details = $form_state['values']['panes']['payment']['details'];

      break;

    case "order-save":
      if(isset($order->payment_details) && isset($order->payment_details['optile-selected-network'])) {
        // Load up the existing data array.
        $data = db_query("SELECT data FROM {uc_orders} WHERE order_id = :id", array(':id' => $order->order_id))->fetchField();
        $data = unserialize($data);

        $data['optile-selected-network'] = $order->payment_details['optile-selected-network'];

        // Save it again.
        db_update('uc_orders')
          ->fields(array('data' => serialize($data)))
          ->condition('order_id', $order->order_id)
          ->execute();
      }

      break;

    case 'customer-view':
      $build = array();

      if(isset($order->data['optile-selected-network']))
        $build['#markup'] = t('Payment Network') . ': '
                .$order->data['optile-selected-network'];

      return $build;

      break;
  }

  return $form;
}

function uc_optile_uc_checkout_pane_alter(&$panes) {
  $panes['payment']['callback'] = 'uc_optile_uc_checkout_pane_payment';
}

/**
 * @file
 * Checkout pane functions for uc_payment.module.
 *
 * The checkout pane holds form to select the payment method. It also shows a
 * preview of the line items and order total.
 */

function uc_optile_uc_checkout_pane_payment($op, &$order, $form = NULL, &$form_state = NULL) {
  $errors = form_get_errors();

  switch ($op) {
    case 'view':
      $contents['#attached']['css'][] = drupal_get_path('module', 'uc_payment') . '/uc_payment.css';

      if (variable_get('uc_payment_show_order_total_preview', TRUE)) {
        $contents['line_items'] = array(
          '#theme' => 'uc_payment_totals',
          '#order' => $order,
          '#prefix' => '<div id="line-items-div">',
          '#suffix' => '</div>',
          '#weight' => -20,
        );
      }

      // Ensure that the form builder uses #default_value to determine which
      // button should be selected after an ajax submission. This is
      // necessary because the previously selected value may have become
      // unavailable, which would result in an invalid selection.
      unset($form_state['input']['panes']['payment']['payment_method']);

      $options = array();
      foreach (_uc_payment_method_list() as $id => $method) {
        $set = rules_config_load('uc_payment_method_' . $method['id']);
        if ($set && !$set->execute($order)) {
          continue;
        }

        if ($method['checkout'] && !isset($method['express'])) {
          $options[$id] = $method['title'];
        }
      }

      drupal_alter('uc_payment_method_checkout', $options, $order);

      $description = '';

      $all_required_fields_entered = uc_optile_check_required_fields($form, $form_state, $order);

      if($all_required_fields_entered) {
        if (!$options) {
          $description = t('Checkout cannot be completed without any payment methods enabled. Please contact an administrator to resolve the issue.');
          $options[''] = t('No payment methods available');
        }
        elseif (count($options) > 1) {
          $description = t('Select a payment method from the following options.');
        }
      } else {
        $description = t('All required fields need to be entered before payment methods are enabled.');
        $options = array();
        $options[''] = t('No payment methods currently available');
      }

      if (!isset($options[$order->payment_method])) {
        // preselects optile payment method
        if(isset($options['optile'])) {
          $order->payment_method = 'optile';
        } else {
          $order->payment_method = key($options);
        }
      }

      $contents['payment_method'] = array(
        '#type' => 'radios',
        '#title' => t('Payment method'),
        '#title_display' => 'invisible',
        '#options' => $options,
        '#default_value' => $order->payment_method,
        '#disabled' => count($options) == 1,
        '#required' => TRUE,
        '#ajax' => array(
          'callback' => 'uc_payment_checkout_payment_details',
          'wrapper' => 'payment-details',
          'progress' => array(
            'type' => 'throbber',
          ),
        ),
      );

      $contents['details'] = array(
        '#prefix' => '<div id="payment-details" class="clearfix payment-details-' . $order->payment_method . '">',
        '#markup' => t('Continue with checkout to complete payment.'),
        '#suffix' => '</div>',
      );

      $func = _uc_payment_method_data($order->payment_method, 'callback');
      if (function_exists($func) && $details = $func('cart-details', $order, $form, $form_state)) {
        unset($contents['details']['#markup']);
        $contents['details'] += $details;
      }

      return array('description' => $description, 'contents' => $contents);

    default:
      return uc_checkout_pane_payment($op, $order, $form, $form_state);
  }
}


/**
 * Implements hook_form_FORM_ID_alter().
 */
function uc_optile_form_uc_cart_checkout_form_alter(&$form, &$form_state, $form_id) {
  $wrapper_div_id = 'uc_optile_checkout_form';

  $form['#prefix'] = "<div id='".$wrapper_div_id."'>";
  $form['#suffix'] = "</div>";

//  // Defining another form submit listener.
  $form['actions']['continue'] = array(
    '#ajax' => array(
        'callback' => 'uc_optile_form_callback',
        'wrapper' => $wrapper_div_id,
        'name' => 'uc_optile_submit',
        'method' => 'replace',
    ),
    '#type' => 'submit',
    '#value' => t('Submit order'),
  );

  $form['#process'][] = 'uc_optile_ajax_process_form';
  array_unshift($form['#validate'], 'uc_optile_uc_cart_checkout_form_validate');
  $form_state['original_submit_handlers'] = $form['#submit'];
  $form['#submit'] = array('uc_optile_uc_cart_checkout_form_submit');
}

/**
 * The method adds saving/reloading of optile payment data in the frontend
 * before the checkout pane update is triggered by the modification of
 * some fields (billing, delivery, ..).
 *
 * @param type $form
 * @param type $form_state
 */
function uc_optile_ajax_process_form($form, &$form_state) {
  foreach ($form_state['uc_ajax'] as $module => $fields) {
    foreach($fields as $key => $panes) {
      if(!isset($panes['payment-pane'])) continue;

      array_unshift($form_state['uc_ajax'][$module][$key], 'uc_optile_save_payment_data');
    }
  }

  return $form;
}

/**
 * Calls JS method from the uc_optile.js
 *
 * @param type $form
 * @param type $form_state
 * @return type
 */
function uc_optile_save_payment_data($form, &$form_state) {
  $commands = array(
    array('command' => 'saveOptileData'),
  );

  return array('#type' => 'ajax', '#commands' => $commands);
}

function uc_optile_form_callback($form, &$form_state) {

  if(!form_get_errors() && $form_state['values']['panes']['payment']['details']['optile-just-want-validation'] == '1') {
//    debug("ajax callback");
    $commands = array(
      array('command' => 'optileDrupalValidationPassed'),
    );

    return array('#type' => 'ajax', '#commands' => $commands);
  }

  if(!form_get_errors() && $form_state['no_redirect'] == 1 && ($form_state['redirect'] == 'cart/checkout/review')) {
    ctools_include('ajax');
    ctools_add_js('ajax-responder');
    $commands = array(ctools_ajax_command_redirect($form_state['redirect']));
//    return array('#type' => 'ajax', '#commands' => $commands);
    print ajax_render($commands);

    // standard drupal_exit() calls exit hooks, which unsets the sescrd in case of credit module
    // don't do that
    //drupal_exit();
    if (drupal_get_bootstrap_phase() == DRUPAL_BOOTSTRAP_FULL) {
       drupal_session_commit();
    }
    exit;
  }
  return $form;
}

function uc_optile_uc_cart_checkout_form_validate($form, &$form_state) {
}

function uc_optile_check_required_fields($form, &$form_state, $order) {
  $ok = true;
  // ugly sorry
  if(isset($form_state['build_info']['args'][0]->primary_email)) {
    if(strlen($form_state['build_info']['args'][0]->primary_email) == 0
             || !valid_email_address($form_state['build_info']['args'][0]->primary_email)) $ok = false;
  }
  if((uc_order_is_shippable($order) || !variable_get('uc_cart_delivery_not_shippable', TRUE)) && isset($form_state['build_info']['args'][0]->delivery_first_name)) {
    if(strlen($form_state['build_info']['args'][0]->delivery_first_name) == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->delivery_last_name) == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->delivery_street1) == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->delivery_city) == 0) $ok = false;
    if($form_state['build_info']['args'][0]->delivery_zone == 0) $ok = false;
    if($form_state['build_info']['args'][0]->delivery_country == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->delivery_postal_code) == 0) $ok = false;

    /**
     * fix for this issue:
     * When you change the country in the checkout, the 'state' is not
     * cleared yet and list request can be done with a wrong state. The state
     * gets cleared when the form is rerendered in the checkout. We'll fix
     * by checking if the selected delivery_zone is inside delivery_country
     */
    if($ok) {
      $zone = uc_optile_load_zone($form_state['build_info']['args'][0]->delivery_zone);
      if($zone['zone_country_id'] != $form_state['build_info']['args'][0]->delivery_country) {
        $ok = false;
      }
    }
  }
  if(isset($form_state['build_info']['args'][0]->billing_first_name)) {
    if(strlen($form_state['build_info']['args'][0]->billing_first_name) == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->billing_last_name) == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->billing_street1) == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->billing_city) == 0) $ok = false;
    if($form_state['build_info']['args'][0]->billing_zone == 0) $ok = false;
    if($form_state['build_info']['args'][0]->billing_country == 0) $ok = false;
    if(strlen($form_state['build_info']['args'][0]->billing_postal_code) == 0) $ok = false;

    /**
     * fix for this issue:
     * When you change the country in the checkout, the 'state' is not
     * cleared yet and list request can be done with a wrong state. The state
     * gets cleared when the form is rerendered in the checkout. We'll fix
     * by checking if the selected delivery_zone is inside delivery_country
     */
    if($ok) {
      $zone = uc_optile_load_zone($form_state['build_info']['args'][0]->billing_zone);
      if($zone['zone_country_id'] != $form_state['build_info']['args'][0]->billing_country) {
        $ok = false;
      }
    }
  }
  return $ok;
}

/**
 * Submit handler - skips the order review if the payment method is Optile.
 */
function uc_optile_uc_cart_checkout_form_submit($form, &$form_state) {
    $order = uc_order_load($_SESSION['cart_order']);

    if ($order->payment_method == 'optile') {
      if($form_state['values']['panes']['payment']['details']['optile-just-want-validation'] == '1') {
//        debug("doing nothing in _submit handler");
//        // do nothing?
      } else {
        if($form_state['values']['panes']['payment']['details']['optile-proceed'] == 1) {
          $form['#submit'] = $form_state['original_submit_handlers'];
          form_execute_handlers('submit', $form, $form_state);

          $form_state['values']['op'] = t('Submit order');
          $form_state['uc_order'] = $order;
          drupal_form_submit('uc_cart_checkout_review_form', $form_state);
          $form_state['programmed'] = FALSE;
          //$form_state['redirect'] = 'cart/optile/review';
        } else {
          $form_state['rebuild'] = true;
        }
      }
    } else {
      $form['#submit'] = $form_state['original_submit_handlers'];
      form_execute_handlers('submit', $form, $form_state);
    }

}
