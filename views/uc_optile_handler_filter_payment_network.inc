<?php

/**
 * @file
 * Views handler.
 */

/**
 * Filters by payment network.
 */
class uc_optile_handler_filter_payment_network extends views_handler_filter_in_operator {

  /**
   * Overrides views_handler_filter_in_operator::get_value_options().
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Payment network');
      $this->value_options = $this->payment_network_list();
    }
  }
  
  /**
   * Returns a list of payment networks.
   */
  function payment_network_list() {
    $result = db_query("SELECT * FROM {uc_optile_order_data} GROUP BY payment_network");

    $options = array();
    while ($row = $result->fetchAssoc()) {
      $options[$row['payment_network']] = t($row['payment_network']);
    }
    if (count($options) == 0) {
      $options[] = t('No payment networks found.');
    }
    natcasesort($options);

    return $options;
  }

}
