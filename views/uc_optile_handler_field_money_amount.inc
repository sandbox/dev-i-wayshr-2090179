<?php

/**
 * @file
 * Views handler: Product price field.
 */

/**
 * Returns a formatted price value to display in the View.
 */
class uc_optile_handler_field_money_amount extends uc_product_handler_field_price {

  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    $order = uc_order_load($values->order_id);
    
    if($order == false){
        return "";
    }

    if ($this->options['format'] == 'numeric') {
      return parent::render($order->order_total);
    }

    if ($this->options['format'] == 'uc_price') {
      return uc_currency_format($order->order_total);
    }
  }

  function query() {

  }

}
