<?php

/**
 * @file
 * Views handler: Full name field handler (first and last).
 */

/**
 * Returns the customer's full name.
 */
class uc_optile_handler_field_order_fullname extends views_handler_field {

  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    $order = uc_order_load($values->order_id);
    if($order == false){
        return "";
    }
    return $order->billing_first_name." ".$order->billing_last_name;
  }

  function query() {

  }

}
