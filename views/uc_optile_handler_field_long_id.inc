<?php

/**
 * @file
 * Views handler: Order status field.
 */

/**
 * Returns a human readable text for order status to display in the View.
 */
class uc_optile_handler_field_long_id extends views_handler_field {
  
  /**
   * Override init function to provide link to Optile monitor.
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
    $this->additional_fields['monitor_link'] = array('table' => 'uc_optile_order_data', 'field' => 'monitor_link');
    $this->additional_fields['long_id'] = array('table' => 'uc_optile_order_data', 'field' => 'long_id');
  }  

/**
   * Renders whatever the data is as a link to the order.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render_link($data, $values) {
    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = $this->get_value($values, 'monitor_link');

//    if (user_access('view all orders')) {
//      $path = 'admin/store/orders/' . $this->get_value($values, 'order_id');
//    }
//    elseif (user_access('view own orders') && $this->get_value($values, 'uid') == $GLOBALS['user']->uid) {
//      $path = 'user/' . $GLOBALS['user']->uid . '/orders/' . $this->get_value($values, 'order_id');
//    }
//    else {
//      $path = FALSE;
//    }
//
//    if ($path && $data !== NULL && $data !== '') {
//      $this->options['alter']['make_link'] = TRUE;
//      $this->options['alter']['path'] = $path;
//    }
    return $data;
  }  
  
  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    return $this->render_link(check_plain($values->{$this->field_alias}), $values);
  }  
  
}
