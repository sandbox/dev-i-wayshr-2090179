<?php

function uc_optile_views_default_views() {

  $view = new view;
  $view->name = 'uc_optile';
  $view->description = '';
  $view->tag = 'Ubercart';
  $view->base_table = 'uc_optile_order_data';
  $view->human_name = '';
  $view->core = 0;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Defaults */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->display->display_options['title'] = 'Optile Orders';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view all orders';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'order_id' => 'order_id',
    'long_id' => 'long_id',
    'date_created' => 'order_id',
    'billing_full_name' => 'order_id',
    'total_order' => 'order_id',
    'status_order' => 'order_id',
    'payment_network' => 'payment_network',
//    'created' => 'created',
  );
  $handler->display->display_options['style_options']['default'] = 'order_id';
  $handler->display->display_options['style_options']['info'] = array(
//    'actions' => array(
//      'align' => '',
//      'separator' => '',
//    ),
    'order_id' => array(
      'sortable' => 1,
      'align' => '',
      'separator' => '',
    ),
    'monitor_link' => array(
      'align' => '',
      'separator' => '',
    ),
    'created' => array(
//      'sortable' => 1,
      'align' => '',
      'separator' => '',
    ),
    'billing_full_name' => array(
      'align' => '',
      'separator' => '',
    ),
    'order_total' => array(
//      'sortable' => 1,
      'align' => '',
      'separator' => '',
    ),
    'order_status' => array(
//      'sortable' => 1,
      'align' => '',
      'separator' => '',
    ),
    'payment_network' => array(
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 1;
  $handler->display->display_options['style_options']['order'] = 'desc';
//  /* Field: Order: Actions */
//  $handler->display->display_options['fields']['actions']['id'] = 'actions';
//  $handler->display->display_options['fields']['actions']['table'] = 'uc_optile_order_data';
//  $handler->display->display_options['fields']['actions']['field'] = 'actions';
//  $handler->display->display_options['fields']['actions']['alter']['alter_text'] = 0;
//  $handler->display->display_options['fields']['actions']['alter']['make_link'] = 0;
//  $handler->display->display_options['fields']['actions']['alter']['absolute'] = 0;
//  $handler->display->display_options['fields']['actions']['alter']['word_boundary'] = 1;
//  $handler->display->display_options['fields']['actions']['alter']['ellipsis'] = 1;
//  $handler->display->display_options['fields']['actions']['alter']['strip_tags'] = 0;
//  $handler->display->display_options['fields']['actions']['alter']['trim'] = 0;
//  $handler->display->display_options['fields']['actions']['alter']['html'] = 0;
//  $handler->display->display_options['fields']['actions']['hide_empty'] = 0;
//  $handler->display->display_options['fields']['actions']['empty_zero'] = 0;

  /* Field: Order: Order ID */
  $handler->display->display_options['fields']['order_id']['id'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['order_id']['field'] = 'order_id';
  $handler->display->display_options['fields']['order_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['order_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['order_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['order_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['order_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['order_id']['link_to_order'] = 1;
  /* Field: Order: Optile Long ID */
  $handler->display->display_options['fields']['long_id']['id'] = 'long_id';
  $handler->display->display_options['fields']['long_id']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['long_id']['field'] = 'long_id';
  $handler->display->display_options['fields']['long_id']['label'] = 'Optile Long ID';
  $handler->display->display_options['fields']['long_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['long_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['long_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['long_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['long_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['long_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['long_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['long_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['long_id']['hide_alter_empty'] = 0;
  /* Field: User: Uid */
//  $handler->display->display_options['fields']['uid']['id'] = 'uid';
//  $handler->display->display_options['fields']['uid']['table'] = 'users';
//  $handler->display->display_options['fields']['uid']['field'] = 'uid';
//  $handler->display->display_options['fields']['uid']['label'] = '';
//  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
//  $handler->display->display_options['fields']['uid']['alter']['alter_text'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['text'] = '[billing_full_name]';
//  $handler->display->display_options['fields']['uid']['alter']['make_link'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['path'] = '[uid]';
//  $handler->display->display_options['fields']['uid']['alter']['absolute'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['external'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['replace_spaces'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['trim_whitespace'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['nl2br'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['word_boundary'] = 1;
//  $handler->display->display_options['fields']['uid']['alter']['ellipsis'] = 1;
//  $handler->display->display_options['fields']['uid']['alter']['strip_tags'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['trim'] = 0;
//  $handler->display->display_options['fields']['uid']['alter']['html'] = 0;
//  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
//  $handler->display->display_options['fields']['uid']['element_default_classes'] = 1;
//  $handler->display->display_options['fields']['uid']['hide_empty'] = 0;
//  $handler->display->display_options['fields']['uid']['empty_zero'] = 0;
//  $handler->display->display_options['fields']['uid']['hide_alter_empty'] = 1;
//  $handler->display->display_options['fields']['uid']['link_to_user'] = 0;
  /* Field: Order: Creation date */
  $handler->display->display_options['fields']['created']['id'] = 'date_created';
  $handler->display->display_options['fields']['created']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Purchase date';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['created']['date_format'] = 'uc_store';
  /* Field: Order: Billing address: Full name */
  $handler->display->display_options['fields']['billing_first_name']['id'] = 'billing_full_name';
  $handler->display->display_options['fields']['billing_first_name']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['billing_first_name']['field'] = 'billing_full_name';
  $handler->display->display_options['fields']['billing_first_name']['label'] = 'Customer';
  $handler->display->display_options['fields']['billing_first_name']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['make_link'] = 0;
//  $handler->display->display_options['fields']['billing_first_name']['alter']['path'] = 'user/[uid]';
  $handler->display->display_options['fields']['billing_first_name']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['external'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['billing_first_name']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['billing_first_name']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['alter']['html'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['billing_first_name']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['billing_first_name']['hide_empty'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['empty_zero'] = 0;
  $handler->display->display_options['fields']['billing_first_name']['hide_alter_empty'] = 1;
  /* Field: Order: Order total */
  $handler->display->display_options['fields']['order_total']['id'] = 'total_order';
  $handler->display->display_options['fields']['order_total']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['order_total']['field'] = 'order_total';
  $handler->display->display_options['fields']['order_total']['label'] = 'Total';
  $handler->display->display_options['fields']['order_total']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['external'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['order_total']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['order_total']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['order_total']['alter']['html'] = 0;
  $handler->display->display_options['fields']['order_total']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['order_total']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['order_total']['hide_empty'] = 0;
  $handler->display->display_options['fields']['order_total']['empty_zero'] = 0;
  $handler->display->display_options['fields']['order_total']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['order_total']['set_precision'] = 0;
  $handler->display->display_options['fields']['order_total']['precision'] = '0';
  $handler->display->display_options['fields']['order_total']['format_plural'] = 0;
//  /* Field: Order: Order status */
  $handler->display->display_options['fields']['order_status']['id'] = 'status_order';
  $handler->display->display_options['fields']['order_status']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['order_status']['field'] = 'order_status';
  $handler->display->display_options['fields']['order_status']['label'] = 'Status';
  $handler->display->display_options['fields']['order_status']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['external'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['order_status']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['order_status']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['order_status']['alter']['html'] = 0;
  $handler->display->display_options['fields']['order_status']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['order_status']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['order_status']['hide_empty'] = 0;
  $handler->display->display_options['fields']['order_status']['empty_zero'] = 0;
  $handler->display->display_options['fields']['order_status']['hide_alter_empty'] = 0;
  /* Field: Order: Optile Payment Network */
  $handler->display->display_options['fields']['payment_network']['id'] = 'payment_network';
  $handler->display->display_options['fields']['payment_network']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['fields']['payment_network']['field'] = 'payment_network';
  $handler->display->display_options['fields']['payment_network']['label'] = 'Payment Network';
  $handler->display->display_options['fields']['payment_network']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['external'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['payment_network']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['payment_network']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['payment_network']['alter']['html'] = 0;
  $handler->display->display_options['fields']['payment_network']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['payment_network']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['payment_network']['hide_empty'] = 0;
  $handler->display->display_options['fields']['payment_network']['empty_zero'] = 0;
  $handler->display->display_options['fields']['payment_network']['hide_alter_empty'] = 0;
  /* Filter criterion: Order: Order ID */
  $handler->display->display_options['filters']['order_id']['id'] = 'order_id';
  $handler->display->display_options['filters']['order_id']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['filters']['order_id']['field'] = 'order_id';
  $handler->display->display_options['filters']['order_id']['group'] = 0;
  $handler->display->display_options['filters']['order_id']['exposed'] = TRUE;
  $handler->display->display_options['filters']['order_id']['expose']['operator_id'] = 'order_id_op';
  $handler->display->display_options['filters']['order_id']['expose']['label'] = 'View order number';
  $handler->display->display_options['filters']['order_id']['expose']['operator'] = 'order_id_op';
  $handler->display->display_options['filters']['order_id']['expose']['identifier'] = 'order_id';
  /* Filter criterion: Order: Order status */
  $handler->display->display_options['filters']['payment_network']['id'] = 'payment_network';
  $handler->display->display_options['filters']['payment_network']['table'] = 'uc_optile_order_data';
  $handler->display->display_options['filters']['payment_network']['field'] = 'payment_network';
//  $handler->display->display_options['filters']['payment_network']['value'] = array(
//    '_active' => '_active',
//  );
  $handler->display->display_options['filters']['payment_network']['group'] = 0;
  $handler->display->display_options['filters']['payment_network']['exposed'] = TRUE;
  $handler->display->display_options['filters']['payment_network']['expose']['operator_id'] = 'payment_network_op';
  $handler->display->display_options['filters']['payment_network']['expose']['label'] = 'View by payment network';
  $handler->display->display_options['filters']['payment_network']['expose']['operator'] = 'payment_network_op';
  $handler->display->display_options['filters']['payment_network']['expose']['identifier'] = 'payment_network';
  $handler->display->display_options['filters']['payment_network']['expose']['reduce'] = 0;

//  /* Filter criterion: Order: Order status */
//  $handler->display->display_options['filters']['order_status']['id'] = 'order_status';
//  $handler->display->display_options['filters']['order_status']['table'] = 'uc_optile_order_data';
//  $handler->display->display_options['filters']['order_status']['field'] = 'order_status';
//  $handler->display->display_options['filters']['order_status']['value'] = array(
//    '_active' => '_active',
//  );
//  $handler->display->display_options['filters']['order_status']['group'] = 0;
//  $handler->display->display_options['filters']['order_status']['exposed'] = TRUE;
//  $handler->display->display_options['filters']['order_status']['expose']['operator_id'] = 'order_status_op';
//  $handler->display->display_options['filters']['order_status']['expose']['label'] = 'View by status';
//  $handler->display->display_options['filters']['order_status']['expose']['operator'] = 'order_status_op';
//  $handler->display->display_options['filters']['order_status']['expose']['identifier'] = 'order_status';
//  $handler->display->display_options['filters']['order_status']['expose']['reduce'] = 0;


  /* Display: Admin page */
  $handler = $view->new_display('page', 'Admin page', 'admin_page');
  $handler->display->display_options['path'] = 'admin/store/orders/optile';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'View Optile orders';
  $handler->display->display_options['menu']['description'] = 'View orders processed through Optile.';
  $handler->display->display_options['menu']['weight'] = '-7';
  $handler->display->display_options['menu']['name'] = 'management';

  $views[$view->name] = $view;

  return $views;
}


