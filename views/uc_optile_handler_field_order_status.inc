<?php

/**
 * @file
 * Views handler: Order status field.
 */

/**
 * Returns a human readable text for order status to display in the View.
 */
class uc_optile_handler_field_order_status extends views_handler_field {

  /**
   * Overrides views_handler_field::render().
   */
  function render($values) {
    $order = uc_order_load($values->order_id);

    if($order == false){
        return "";
    }

    return check_plain(uc_order_status_data($order->order_status, 'title'));
  }

  function query() {

  }

}
