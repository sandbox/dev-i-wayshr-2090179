(function($) {
  /**
   * Initialization
   */
  Drupal.behaviors.uc_optile_prevent_js_alerts = {
    /**
     * Run Drupal module JS initialization.
     *
     * @param context
     * @param settings
     */
    attach: function(context, settings) {
      // Override the alert() function.
      window.alert = function(text) {
        if (typeof console != "undefined" && text.indexOf("terminated abnormally.") !== -1) {
          console.error("Module 'uc_optile' prevented the following alert: " + text);
        }
        return true;
      };
    }
  };
})(jQuery);