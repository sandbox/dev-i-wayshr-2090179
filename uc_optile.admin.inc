<?php

/**
 * Callback for payment gateway settings.
 */
function uc_optile_settings_form($form, &$form_state) {
  $form['optile_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Optile settings'),
    '#description' => t(<<<DESCRIPTION
            <style>
              #links-contact td, #links-contact th {
                padding: 1px 10px;
              }
            </style>
            <table id="links-contact" style="width: 100%">
              <tbody>
                <tr>
                    <td><strong>Links:</strong></td>
                    <td><strong>Contact:</strong></td>
                </tr>
                <tr>
                    <td><a target="_blank" href="http://www.optile.de/group/docs/home">Ubercart Optile extension user guide</a></td>
                    <td><a target="_blank" href="http://www.optile.de/web/guest/contact-us">Leave message via Optile contact form</a></td>
                </tr>
                <tr>
                    <td><a target="_blank" href="https://oscato.com/monitor/">Optile transaction monitor</a></td>
                    <td><a href="mailto:support@optile.zendesk.com">E-mail: support@optile.zendesk.com</a></td>
                </tr>
                <tr>
                    <td><a target="_blank" href="https://sandbox.oscato.com/monitor/">Optile transaction monitor (sandbox)</a></td>
                    <td>&nbsp;</td>
                </tr>
              </tbody>
            </table>
DESCRIPTION
    ),
  );

  $form['optile_settings']['uc_optile_merchant_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Optile merchant code'),
    '#size' => 60,
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => variable_get('uc_optile_merchant_code', ''),
  );

  $form['optile_settings']['uc_optile_merchant_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Optile merchant token'),
    '#size' => 60,
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => variable_get('uc_optile_merchant_token', ''),
  );

  $form['optile_settings']['uc_optile_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Optile API URL'),
    '#size' => 60,
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => variable_get('uc_optile_api_url', 'https://sandbox.oscato.com/'),
  );

  $form['optile_settings']['uc_optile_remote_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('Optile Notification IP'),
    '#description' => t('Optile IP that sends payment notifications. Notifications will be accepted only from this IP address.'),
    '#size' => 60,
    '#maxlength' => 254,
    '#required' => TRUE,
    '#default_value' => variable_get('uc_optile_remote_ip', ''),
  );

  $form['optile_settings']['uc_optile_proxy_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('Proxy / Load balancer IP address'),
    '#description' => t('If your server is behind a load balancer or proxy, fill in its IP address here. This should be a trusted IP that belongs to you.'),
    '#size' => 60,
    '#maxlength' => 254,
    '#required' => FALSE,
    '#default_value' => variable_get('uc_optile_proxy_ip', ''),
  );

  // checks if any other payment method is active and gives you an option to
  // disable it
  $methods = _uc_payment_method_list();
  $active_methods = array();

  foreach ($methods as $id => $method) {
    if ($id == 'optile')
      continue;
    $is_active = variable_get('uc_payment_method_' . $id . '_checkout', $method['checkout']);

    if ($is_active)
      $active_methods[$id] = $method['name'];
  }

  if (count($active_methods) > 0) {
    $form['optile_settings']['uc_optile_disable_payment_methods'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable other payment methods'),
      '#description' => '<font color="red">' . t('Other methods are still enabled') . '</font>',
    );

    $form['optile_settings']['uc_optile_selected_payment_methods'] = array(
      '#type' => 'checkboxes',
      '#options' => $active_methods,
      '#title' => t('Select payment methods you wish to disable:'),
      '#states' => array(
        'visible' => array(
          ':input[name="uc_optile_disable_payment_methods"]' => array('checked' => TRUE),
        )
      ),
    );
  }

  $form['#submit'][] = 'uc_optile_settings_form_submit';

  return $form;
}

/**
 * Optile settings form submit handler.
 *
 * @param type $form
 * @param type $form_state
 */
function uc_optile_settings_form_submit($form, &$form_state) {
  // Checks if any (other) payment methods have been selected to be disabled
  if (isset($form_state['values']['uc_optile_disable_payment_methods']) && $form_state['values']['uc_optile_disable_payment_methods']) {
    $networks = $form_state['values']['uc_optile_selected_payment_methods'];
    foreach ($networks as $id => $selected) {
      if ($selected == '0')
        continue;

      variable_set('uc_payment_method_' . $id . '_checkout', 0);
    }
  }
}

/**
 * Implements hook_uc_store_status().
 */
function uc_optile_uc_store_status() {
  // display to the merchant all notifications related to optile orders
  $notifications = uc_optile_get_merchant_notifications();

  foreach ($notifications as $id => $notification) {
    $subject = $notification->subject;
    $message = $notification->message;
    $dismiss = url('admin/uc_optile/merchant_notifications/' . $notification->pid . '/dismiss', array('query' => drupal_get_destination()));
    drupal_set_message(
            t("Optile: <strong>$subject</strong><br />$message<br/><a href='$dismiss'>[Dismiss]</a>"), $notification->type, FALSE);
  }

  $messages = array();
  // Check to see if the merchant code & token have been set.
  if (variable_get('uc_optile_merchant_code', '') == '' || variable_get('uc_optile_merchant_token', '') == '') {
    $messages[] = array(
      'status' => 'error',
      'title' => t('Optile payment gateway'),
      'desc' => t('Merchant code and token have not been set. Please enter them <a href="@url">here</a>.', array('@url' => url('admin/store/settings/payment/method/optile'))),
    );
  } else {
    $messages[] = array(
      'status' => 'ok',
      'title' => t('Optile payment gateway'),
      'desc' => t('Mercant code and token have been set.'),
    );
  }

  return $messages;
}

function uc_optile_get_merchant_notifications() {
  $query = db_select('uc_optile_merchant_notifications', 'n')
          ->fields('n')
          ->where('dismissed = 0');

  return $query->execute()->fetchAll();
}

function uc_optile_add_merchant_notification($subject, $message, $type = 'error') {
  db_insert('uc_optile_merchant_notifications')
          ->fields(array(
            'subject' => $subject,
            'message' => $message,
            'type' => $type,
            'created' => time(),
          ))
          ->execute();
}

/**
 * Displays all the active/dismissed merchant notifications regarding Optile orders.
 */
function uc_optile_view_merchant_notifications() {
  $page = isset($_GET['page']) ? intval($_GET['page']) : 0;
  $page_size = 30;
  $rows = array();

  $header = array(
    array('data' => t('#'), 'field' => 'n.pid', 'sort' => 'desc'),
    array('data' => t('Date created'), 'field' => 'n.created'),
    array('data' => t('Message'), 'field' => "n.message"),
    array('data' => t('Actions')),
  );

  $query = db_select('uc_optile_merchant_notifications', 'n', array('fetch' => PDO::FETCH_ASSOC))->extend('PagerDefault')->extend('TableSort');
  $query->fields('n', array(
    'pid',
    'created',
    'subject',
    'message',
    'dismissed',
  ));

  $count_query = db_select('uc_optile_merchant_notifications', 'n');
  $count_query->addExpression('COUNT(DISTINCT n.pid)');

  $query->setCountQuery($count_query);
  $query
          ->orderByHeader($header)
          ->limit($page_size);

  $merchant_notifications = $query->execute();

  foreach ($merchant_notifications as $notification) {
    $pid = $notification['pid'];
    $created = format_date($notification['created']);
    $subject = $notification['subject'];
    $message = "<strong>$subject</strong><br />" . $notification['message'];

    $actions = array();
    if ($notification['dismissed'] == 0) {
      $actions[] = l(t('Mark as read'), 'admin/uc_optile/merchant_notifications/' . $pid . '/dismiss', array('query' => drupal_get_destination()));
    }
    $actions[] = l(t('Delete'), 'admin/uc_optile/merchant_notifications/' . $pid . '/delete', array('query' => drupal_get_destination()));

    $actions = implode(' | ', $actions);

    $rows[] = array(
      array('data' => $pid),
      array('data' => $created),
      array('data' => $message),
      array('data' => $actions),
    );
  }

  $build['report'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('width' => '100%'),
    '#empty' => t('No merchant notifications available'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Marks the notification as read and returns to the referrer.
 *
 * @param int $notification_pid
 */
function uc_optile_dismiss_merchant_notification($notification_pid) {
  db_update('uc_optile_merchant_notifications')
          ->fields(array(
            'dismissed' => time(),
            'updated' => time(),
          ))
          ->condition('pid', $notification_pid)
          ->execute();

  drupal_set_message(t('Message marked as read'));
  drupal_goto(drupal_get_destination());
}

/**
 * Displays all the received from Optile.
 */
function uc_optile_view_optile_notifications() {
  $page = isset($_GET['page']) ? intval($_GET['page']) : 0;
  $page_size = 30;
  $rows = array();
  $api_url = rtrim(variable_get('uc_optile_api_url'), '/');

  $header = array(
    array('data' => t('Notification #'), 'field' => 'n.id', 'sort' => 'desc'),
    array('data' => t('Transaction #'), 'field' => 'n.transaction_id'),
    array('data' => t('Date received')),
    array('data' => t('Optile Long Id'), 'field' => 'n.long_id'),
    array('data' => t('Return code'), 'field' => 'n.return_code'),
    array('data' => t('Interaction code'), 'field' => 'n.interaction_code'),
    array('data' => t('Reason code'), 'field' => 'n.reason_code'),
    array('data' => t('Result info'), 'field' => 'n.result_info'),
    array('data' => t('Status code returned to Optile'), 'field' => 'n.status'),
  );

  $query = db_select('uc_optile_notifications', 'n', array('fetch' => PDO::FETCH_ASSOC))->extend('PagerDefault')->extend('TableSort');
  $query->fields('n', array(
    'id',
    'transaction_id',
    'date',
    'long_id',
    'return_code',
    'interaction_code',
    'reason_code',
    'result_info',
    'status',
  ));

  $count_query = db_select('uc_optile_notifications', 'n');
  $count_query->addExpression('COUNT(DISTINCT n.id)');

  $query->setCountQuery($count_query);
  $query
          ->orderByHeader($header)
          ->limit($page_size);

  $ipns = $query->execute();

  foreach ($ipns as $ipn) {
    $id = l($ipn['id'], 'admin/store/orders/optile_notifications/' . $ipn['id'] . '/view');
    $transaction_id = $ipn['transaction_id'];
    $date = format_date($ipn['date']);
    $long_id = "<a href='$api_url/monitor/transactions/".$ipn['long_id']."'>".$ipn['long_id']."</a>";
    $return_code = $ipn['return_code'];
    $interaction_code = $ipn['interaction_code'];
    $reason_code = $ipn['reason_code'];
    $result_info = $ipn['result_info'];
    $status = $ipn['status'];

    $rows[] = array(
      array('data' => $id),
      array('data' => $transaction_id),
      array('data' => $date),
      array('data' => $long_id),
      array('data' => $return_code),
      array('data' => $interaction_code),
      array('data' => $reason_code),
      array('data' => $result_info),
      array('data' => $status),
    );
  }

  $build['report'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('width' => '100%'),
    '#empty' => t('No payment notifications available'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );

  return $build;
}

/**
 * Displays single IPN from Optile
 *
 * @param int $notification_id
 */
function uc_optile_view_optile_notification($notification_id) {
  $rows = array();

  $header = array(
    array('data' => t('Key')),
    array('data' => t('Value')),
  );

  $result = db_query('SELECT * FROM {uc_optile_notifications} n WHERE n.id = :id', array(':id' => $notification_id));
  foreach ($result as $record) {
    $data = json_decode($record->received_data, true);

    foreach($data as $k => $v) {
      $rows[] = array(
        array('data' => "<strong>$k</strong>"),
        array('data' => $v),
      );
    }
  }

  $build['report'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('width' => '100%'),
    '#empty' => t('No data available'),
  );

  return $build;
}

/**
 * Delete the merchant notification return to the referrer.
 *
 * @param int $notification_pid
 */
function uc_optile_delete_merchant_notification($notification_pid) {
  db_delete('uc_optile_merchant_notifications')
          ->condition('pid', $notification_pid)
          ->execute();

  drupal_set_message(t('Message deleted'));
  drupal_goto(drupal_get_destination());
}
