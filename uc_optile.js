/**
 * @file
 * Optile JS methods
 */
jQuery(document).ready(function () {
  if(Drupal.ajax) {
    Drupal.ajax.prototype.commands.saveOptileData = function() {
      if(jQuery('.optile-network').length !== 0) {
        window.loadOptileDataAfterBillingDeliveryModification = true;
        saveOptileData();
        jQuery('.optile-network').find('input, textarea, button, select').attr('disabled',false);
      }
    };

    /**
     * Handler for the form serialization.
     * Overriden by Optile so that optile payment network data is disabled
     * before sending them by ajax to drupal.
     *
     * Runs before the beforeSend() handler (see below), and unlike that one, runs
     * before field data is collected.
     */
    Drupal.ajax.prototype.beforeSerialize = function (element, options) {
      jQuery('.optile-network').find('input, textarea, button, select').attr('disabled','disabled');

      // Allow detaching behaviors to update field values before collecting them.
      // This is only needed when field values are added to the POST data, so only
      // when there is a form such that this.form.ajaxSubmit() is used instead of
      // $.ajax(). When there is no form and $.ajax() is used, beforeSerialize()
      // isn't called, but don't rely on that: explicitly check this.form.
      if (this.form) {
        var settings = this.settings || Drupal.settings;
        Drupal.detachBehaviors(this.form, settings, 'serialize');
      }

      // Prevent duplicate HTML ids in the returned markup.
      // @see drupal_html_id()
      options.data['ajax_html_ids[]'] = [];
      jQuery('[id]').each(function () {
        options.data['ajax_html_ids[]'].push(this.id);
      });

      // Allow Drupal to return new JavaScript and CSS files to load without
      // returning the ones already loaded.
      // @see ajax_base_page_theme()
      // @see drupal_get_css()
      // @see drupal_get_js()
      options.data['ajax_page_state[theme]'] = Drupal.settings.ajaxPageState.theme;
      options.data['ajax_page_state[theme_token]'] = Drupal.settings.ajaxPageState.theme_token;
      for (var key in Drupal.settings.ajaxPageState.css) {
        options.data['ajax_page_state[css][' + key + ']'] = 1;
      }
      for (var key in Drupal.settings.ajaxPageState.js) {
        options.data['ajax_page_state[js][' + key + ']'] = 1;
      }
    };

    Drupal.ajax.prototype.beforeSubmit = function (form_values, element, options) {
//      jQuery('.optile-network').find('input, textarea, button, select').attr('disabled','disabled');
    };

    Drupal.ajax.prototype.commands.optileDrupalValidationPassed = function() {
      // restore payment data that was removed from the html so it's not sent to Drupal
      jQuery('.optile-network').find('input, textarea, button, select').attr('disabled',false);

  //    console.log("Submitting CHARGE to Optile");
      // submit charge request to optile
      var network = validationController.widget.find(validationController.selectedNetworkSelector).val();
      var form = validationController.FindFormByFormId(network);
      var params = form.ToJSON();
      var operationUrl = form.operationUrl;
      var listRequestSelfLink = null;

      jQuery("#uc_optile_checkout_form .form-submit").last().attr('disabled', 'disabled');
      jQuery("#uc_optile_checkout_form .form-submit").last().after('<div class="ajax-progress"><div class="throbber"></div>' + Drupal.t('Please wait...') +'</div>');
      jQuery.post(operationUrl, params, function(response) {
        jQuery("#uc_optile_checkout_form .ajax-progress").remove();
        jQuery("#uc_optile_checkout_form .form-submit").last().attr('disabled', false);
        var paymentResponse = OptilePaymentResponseFactory.newOptilePaymentResponse(response);
        var handler;
        switch (paymentResponse.interaction.code) {
          case OptileInteractionCode.PROCEED:
            handler = new OptileInteractionCodeHandlerPROCEED(paymentResponse, listRequestSelfLink);
            break;

          case OptileInteractionCode.RETRY:
            handler = new OptileInteractionCodeHandlerRETRY(paymentResponse, listRequestSelfLink);
            break;

          case OptileInteractionCode.TRY_OTHER_ACCOUNT:
            handler = new OptileInteractionCodeHandlerTRY_OTHER_ACCOUNT(paymentResponse, listRequestSelfLink);
            break;

          case OptileInteractionCode.TRY_OTHER_NETWORK:
            handler = new OptileInteractionCodeHandlerTRY_OTHER_NETWORK(paymentResponse, listRequestSelfLink);
            break;

          case OptileInteractionCode.ABORT:
            handler = new OptileInteractionCodeHandlerABORT(paymentResponse, listRequestSelfLink);
            break;

          default:
            console.log("ERROR! can't find interaction code handler");
            break;
        }
        if (handler) {
          handler.handle(self);
        }

      });
    }
  }
});

jQuery(document).ready(function() {
  // initialize click handlers on radio buttons so optile payment forms
  // can be shown/hidden based on selected network
  if (jQuery('#payment-details') && window.optileForms) {
    initializeOptileHandlers();
  }
});

jQuery(document).ajaxSuccess(function(event, xhr, settings) {
  if(settings.url.indexOf("system/ajax") === -1) return;
  // reinitialize click handlers on radio buttons for optile payment
  // forms show/hide
  if (jQuery('#payment-details') && window.optileForms) {
    initializeOptileHandlers();
  }

  var jsonResponse = JSON.parse(xhr.responseText);
  if(jsonResponse.length === 3 && jsonResponse[2].method === 'prepend'
          && jQuery('.messages.error').length > 0) {
    jQuery('html, body').animate({
        scrollTop: jQuery("#content").offset().top
    }, 1000);
  }

});

function validateUbercartEmail() {
  var email = jQuery("input[name='panes[customer][primary_email]']");
  email.removeClass('error');

  var ok = true;

  if (!checkMail(email.val())) {
    ok = false;
    email.addClass('error');
    alert(Drupal.t("Please enter a valid email address."));
    jQuery('html, body').animate({
      scrollTop: jQuery('#customer-pane').offset().top
    }, 1000);
  }

  return ok;
}

ValidationController.prototype.ValidationResult = function(resultData, resultStatus, xhr, onSuccess, onFail) {
  var network = this.widget.find(this.selectedNetworkSelector).val();
  var form = this.FindFormByFormId(network);
  form.BindMessages(resultData.messages);

  if (resultData.valid) {
//    console.log("valid");
    optileOnValidationSuccess();
  } else {
//    console.log("invalid");
  }

};

function optileOnValidationSuccess() {
  // validate rest of the checkout form with Drupal first

//  console.log("Submitting order to Drupal");
  // submit the order to ubercart in background
  window.optileDontInterceptSubmit = true;
  saveOptileData();
  var network = window.validationController.widget.find(window.validationController.selectedNetworkSelector).val();
  var form = validationController.FindFormByFormId(network);

  jQuery('.optile-network').find('input, textarea, button, select').attr('disabled','disabled');
  jQuery("input[name='panes[payment][details][optile-just-want-validation]']").attr("value", "1");

  var self = this;
  jQuery(document).bind("ajaxSuccess.optileOnValidationSuccess", function(event, xhr, settings) {
    if(settings.url.indexOf("system/ajax") !== -1) {
      jQuery(document).unbind("ajaxSuccess.optileOnValidationSuccess");
      window.optileDontInterceptSubmit = false;
      jQuery('.optile-network').find('input, textarea, button, select').attr('disabled',false);
      jQuery("input[name='panes[payment][details][optile-just-want-validation]']").attr("value", "0");
    }
  });

  jQuery("#uc_optile_checkout_form .form-submit").last().trigger("mousedown");
}

function initializeOptileHandlers() {
  window.validationController = new ValidationController(jQuery("#payment-details"), window.optileForms, jQuery("#uc-cart-checkout-form"), "input[name=\'panes[payment][details][optile-selected-network]\']:checked");
  window.validationController.widget = jQuery('#payment-details');
  window.validationController.Init();

  jQuery('.payment-details-optile .verificationCode').parent().append('<span class="after">?</span>')

  jQuery('.payment-details-optile .after').hover(
    function() {
      jQuery('.payment-details-optile .hint').show();
    }, function() {
      jQuery('.payment-details-optile .hint').hide();
    }
  );

//  console.log("initializeOptileHandlers");

  // this is saved in the CHARGE on RETRY/TRY_OTHER_ACCOUNT handler
  if((window.optileDontInterceptSubmit || window.loadOptileDataAfterBillingDeliveryModification) && window.savedOptileData) {
//    console.log("reloading data in form");
    var form = window.validationController.FindFormByFormId(window.savedOptileNetworkCode);
    form.BindValues(window.savedOptileData);
    jQuery("[name='panes[payment][payment_method]'][value='" + form.networkCode + "']").click();

    delete window.loadOptileDataAfterBillingDeliveryModification;
    delete window.savedOptileNetworkCode;
    delete window.savedOptileData;
  }

  // intercept submit of the checkout form and do validation of Optile fields
  jQuery("#uc_optile_checkout_form .form-submit").last().unbind('mousedown.optile');
  jQuery("#uc_optile_checkout_form .form-submit").last().bindFirst('mousedown.optile', function(event) {
//    console.log("inside optile mousedown handler");

    var selectedPaymentMethod = jQuery("input[name='panes[payment][payment_method]']:checked").val();
    if(selectedPaymentMethod == 'optile') {
      if (window.optileDontInterceptSubmit) {
        return true;
      }
      event.stopImmediatePropagation();

      // Optile Validation
      //
      // since the validation is async to optile server, validationResult
      // will do charge request if the form is valid
//      console.log("doing Optile validation magic");

  //            validateUbercartEmail();
      validationController.Validate(event);

      return false;
    }

    return true;
  });

  OptileShowHidePaymentForm();
}

function OptileShowHidePaymentForm() {
  jQuery('#payment-details .description').hide();

  // show/hide payment forms based on selected payment network
  var currentSelected = jQuery("input[name=\'panes[payment][details][optile-selected-network]\']:checked");
  if (currentSelected !== undefined) {
    OptileFormChangeVisible(currentSelected);
  }

  jQuery("input[name=\'panes[payment][details][optile-selected-network]\']").change(function() {
    OptileFormChangeVisible(jQuery(this));
  });
}

function OptileFormChangeVisible(currentNetworkRadio) {
  jQuery('#payment-details .description').hide();
  currentNetworkRadio.parent().find('.description').show();
}

function checkMail(email) {
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (filter.test(email)) {
    return true;
  }
  return false;
}

// JS copied from magento connector

/**
 * Handles PROCEED optile payment response
 */
function OptileInteractionCodeHandlerPROCEED(paymentResponse, listRequestSelfLink) {
  this.paymentResponse = paymentResponse;
  this.listRequestSelfLink = listRequestSelfLink;
}

OptileInteractionCodeHandlerPROCEED.prototype = {
  handle: function(context) {
    // submit the order to ubercart in background
    window.optileDontInterceptSubmit = true;
    jQuery('.optile-network').find('input, textarea, button, select').attr('disabled','disabled');
    jQuery("input[name='panes[payment][details][optile-proceed]']").attr("value", "1");

    var self = this;
    jQuery(document).bind("ajaxSuccess.optile", function(event, xhr, settings) {
      if(settings.url.indexOf("system/ajax") !== -1) {
        jQuery(document).unbind("ajaxSuccess.optile");
        window.optileDontInterceptSubmit = false;
        var jsonResponse = JSON.parse(xhr.responseText);
        if(jsonResponse.length == 1) {
          var redirect = self.paymentResponse.redirect;
          switch (redirect.method) {
            case "GET":
              window.location.href = redirect.url;
              break;

            case "POST":
              // hacky way to do a POST redirect in javascript from stackoverflow
              var form = jQuery('<form action="' + redirect.url + '" method="post" style="display:none;">' +
                      '</form>');
              jQuery('body').append(form);
              form.submit();
              break;
          }
        }
      }
    });

    jQuery("#uc_optile_checkout_form .form-submit").last().trigger("mousedown");
  }
};

var OptileErrorHandler = {
  error: function(msg) {
    alert(msg);
  }
}

function refreshCheckoutForm(options) {
    var options = options || {};
    var reloadListRequest = options.reloadListRequest || true;
    var saveAndReloadData = options.saveAndReloadData || false;

    // reload checkout form
    window.optileDontInterceptSubmit = true;
    jQuery('.optile-network').find('input, textarea, button, select').attr('disabled','disabled');
    jQuery("input[name='panes[payment][details][optile-proceed]']").attr("value", "0");

    if(reloadListRequest === true) {
      jQuery("input[name='panes[payment][details][optile-reload-list-request]']").attr("value", "1");
    } else {
      jQuery("input[name='panes[payment][details][optile-reload-list-request]']").attr("value", "0");
    }

    var network = validationController.widget.find(validationController.selectedNetworkSelector).val();
    var form = validationController.FindFormByFormId(network);
    var data = form.ToJSON();
    if(data.hasOwnProperty('verificationCode')) {
      delete data['verificationCode'];
    }

    if(saveAndReloadData === true) {
      saveOptileData();
    }

    var self = this;
    jQuery(document).bind("ajaxSuccess.optile", function(event, xhr, settings) {
      if(settings.url.indexOf("system/ajax") !== -1) {
        jQuery(document).unbind("ajaxSuccess.optile");
        window.optileDontInterceptSubmit = false;
        var jsonResponse = JSON.parse(xhr.responseText);
      }
    });

    jQuery("#uc_optile_checkout_form .form-submit").last().trigger("mousedown");
}

function saveOptileData() {
  var network = validationController.widget.find(validationController.selectedNetworkSelector).val();
  var form = validationController.FindFormByFormId(network);
  var data = form.ToJSON();
  if(data.hasOwnProperty('verificationCode')) {
    delete data['verificationCode'];
  }

  window.savedOptileNetworkCode = network;
  window.savedOptileData = data;
}

/**
 * Handles RETRY response code
 */
function OptileInteractionCodeHandlerRETRY(paymentResponse, listRequestSelfLink) {
  this.paymentResponse = paymentResponse;
  this.listRequestSelfLink = listRequestSelfLink;
}

OptileInteractionCodeHandlerRETRY.prototype = {
  handle: function(context) {

    OptileErrorHandler.error(Drupal.t("Couldn't make a payment") + ": " + this.paymentResponse.resultInfo);

    refreshCheckoutForm({ reloadListRequest: true, saveAndReloadData: true });
  }
};

/**
 * Handles TRY_OTHER_ACCOUNT response code
 */
function OptileInteractionCodeHandlerTRY_OTHER_ACCOUNT(paymentResponse, listRequestSelfLink) {
  this.paymentResponse = paymentResponse;
  this.listRequestSelfLink = listRequestSelfLink;
}

OptileInteractionCodeHandlerTRY_OTHER_ACCOUNT.prototype = {
  handle: function(context) {

    OptileErrorHandler.error(Drupal.t("Couldn't make a payment") + ": " + this.paymentResponse.resultInfo);

    refreshCheckoutForm({ reloadListRequest: true, saveAndReloadData: true });
  }
};

/**
 * Handles TYR_OTHER_NETWORK response code - same as TRY_OTHER_ACCOUNT
 */
function OptileInteractionCodeHandlerTRY_OTHER_NETWORK(paymentResponse, listRequestSelfLink) {
  this.paymentResponse = paymentResponse;
  this.listRequestSelfLink = listRequestSelfLink;
}

OptileInteractionCodeHandlerTRY_OTHER_NETWORK.prototype = {
  handle: function(context) {

    OptileErrorHandler.error(Drupal.t("Couldn't make a payment") + ": " + this.paymentResponse.resultInfo);

    refreshCheckoutForm({ reloadListRequest: true, saveAndReloadData: false });
  }
};

function OptileInteractionCodeHandlerABORT(paymentResponse, listRequestSelfLink) {
  this.paymentResponse = paymentResponse;
  this.listRequestSelfLink = listRequestSelfLink;
}

OptileInteractionCodeHandlerABORT.prototype = {
  handle: function(context) {
    var redirect = this.paymentResponse.redirect;

    OptileErrorHandler.error(Drupal.t("Couldn't make a payment") + ": " + this.paymentResponse.resultInfo);

    switch (redirect.method) {
      case "GET":
        window.location.href = redirect.url;
        break;

      case "POST":
        // hacky way to do a POST redirect in javascript from stackoverflow
        var form = jQuery('<form action="' + redirect.url + '" method="post" style="display:none;">' +
                '</form>');
        jQuery('body').append(form);
        form.submit();
        break;
    }

  }
};

var OptilePaymentResponseFactory = {
  newOptilePaymentResponse: function(responseData) {
    return new OptilePaymentResponse(responseData.links, responseData.info, responseData.interaction, responseData.redirect, responseData.resultInfo);
  }
}


function OptilePaymentResponse(links, info, interaction, redirect, resultInfo) {
  this.links = links;
  this.info = info;
  this.interaction = interaction;
  this.redirect = redirect;
  this.resultInfo = resultInfo;
}
OptilePaymentResponse.prototype = {};

var OptileInteractionCode = {
  PROCEED: 'PROCEED',
  RETRY: 'RETRY',
  TRY_OTHER_ACCOUNT: 'TRY_OTHER_ACCOUNT',
  TRY_OTHER_NETWORK: 'TRY_OTHER_NETWORK',
  ABORT: 'ABORT'
};

/*
 * jQuery.bind-first library v0.2.1
 * Copyright (c) 2013 Vladimir Zhuravlev
 *
 * Released under MIT License
 * @license
 *
 * Date: Thu Jun 13 21:06:55 NOVT 2013
 **/
(function(t){function e(e){return f?e.data("events"):t._data(e[0]).events}function n(t,n,r){var i=e(t),a=i[n];if(!f){var s=r?a.splice(a.delegateCount-1,1)[0]:a.pop();return a.splice(r?0:a.delegateCount||0,0,s),void 0}r?i.live.unshift(i.live.pop()):a.unshift(a.pop())}function r(e,r,i){var a=r.split(/\s+/);e.each(function(){for(var e=0;a.length>e;++e){var r=t.trim(a[e]).match(/[^\.]+/i)[0];n(t(this),r,i)}})}var i=t.fn.jquery.split("."),a=parseInt(i[0]),s=parseInt(i[1]),f=1>a||1==a&&7>s;t.fn.bindFirst=function(){var e=t.makeArray(arguments),n=e.shift();return n&&(t.fn.bind.apply(this,arguments),r(this,n)),this},t.fn.delegateFirst=function(){var e=t.makeArray(arguments),n=e[1];return n&&(e.splice(0,2),t.fn.delegate.apply(this,arguments),r(this,n,!0)),this},t.fn.liveFirst=function(){var e=t.makeArray(arguments);return e.unshift(this.selector),t.fn.delegateFirst.apply(t(document),e),this},f||(t.fn.onFirst=function(e,n){var i=t(this),a="string"==typeof n;if(t.fn.on.apply(i,arguments),"object"==typeof e)for(type in e)e.hasOwnProperty(type)&&r(i,type,a);else"string"==typeof e&&r(i,e,a);return i})})(jQuery);
