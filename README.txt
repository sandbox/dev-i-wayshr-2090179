-- SUMMARY --

uc_optile is a module created to work with Ubercart e-commerce, based on Drupal.
It features payment solution based on Native WITHOUT PCI implementation.

Features

* Initial LIST request/response
* Credit card data validation via AJAX
* CHARGE request via AJAX
* IPN handler for order completion

-- INSTALLATION --

Standard installation, see https://drupal.org/node/70151

After the installation, modify Optile payment gateway settings in:
Modules -> Payment -> Optile -> Configure
