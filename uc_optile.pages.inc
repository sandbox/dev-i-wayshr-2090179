<?php

require_once dirname(__FILE__) . '/uc_optile.admin.inc';
require_once dirname(__FILE__) . '/uc_optile.api.inc';

/**
 * @file
 * Optile additional controllers for review and IPN steps in ubercart checkout.
 */

/**
 * Renders optile payment forms in the review step.
 *
 * @param array $form
 *   drupal form object
 * @param array $form_state
 *   drupal form state
 * @param object $order
 *   order object
 *
 * @return array
 *   returns updated form object
 */
function uc_optile_payment_form($form, &$form_state, $order) {
  $order->payment_details = array('');
  $billing_country = uc_optile_load_country($order->billing_country);

  list($merchant_code, $token, $url) = uc_optile_get_keys();

  $request = new UcOptileListRequest();
  $request->merchantCode = $merchant_code;
  $request->merchantToken = $token;
  $request->country = $billing_country['country_iso_code_2'];
  $request->transactionId = $order->order_id;

  $customer = new UcOptileCustomer();

  $email = $order->primary_email != "" ? $order->primary_email : 'checkout@mysite.com';
  $uid = $order->uid != 0 ? $order->uid : 1;

  $customer->email = $email;
  $customer->number = $uid;

  $request->customer = $customer;

  $callback = new UcOptileCallback();
  $callback->cancelUrl = url('cart', array('absolute' => TRUE));
  $callback->notificationUrl = url('uc_optile/ipn', array('absolute' => TRUE));
  $callback->returnUrl = url('uc_optile/return', array('absolute' => TRUE));

  $request->callback = $callback;

  $payment = new UcOptilePayment();
  $payment->amount = $order->order_total;
  $payment->currency = $order->currency;
  $payment->reference = "Order $order->order_id";

  $request->payment = $payment;

  array_walk($order->products, function ($data, $id) use ($order, $request) {
    $product = new UcOptileProduct();
    $product->amount = $data->price * $data->qty;
    $product->code = $data->model;
    $product->currency = $order->currency;
    $product->name = $data->title . ' x' . $data->qty;
    $request->addProduct($product);
  });

  $result = $request->GetResponse($url);
  $networks = $result->networks;
  $networks_formatted = array();

  foreach ($networks['CREDIT_CARD'] as $group) {
    $networks_formatted[$group->code] = '<img src="' . $group->links->logoLink . '" />';
  }

  $form["optile-selected-network"] = array(
    '#type' => 'radios',
    '#title' => t("Select payment network"),
    '#options' => $networks_formatted,
    // N: preselects first network.
    '#default_value' => count($networks_formatted) > 0 ? key($networks_formatted) : NULL,
  );

  foreach ($networks['CREDIT_CARD'] as $group) {
    $form['optile-selected-network'][$group->code]['#description'] = str_ireplace('${formId}', $group->code, '<div class="optile-network" id="optile-' . $group->code . '-network" >' . $group->links->localizedFormHtml . '</div>');
  }

  $optile_forms_js = "window.optileForms = [];";

  foreach ($networks['CREDIT_CARD'] as $group) {
    $optile_forms_js .= <<<JS
            window.optileForms.push(new OptilePaymentNetworkForm('{$group->code}', '{$group->links->validationLink}', '{$group->links->operation}'));
JS;
  }

  $form['uc_optile_executor'] = array(
    '#markup' => '<script type="text/javascript">'
    . $optile_forms_js .
    '</script>',
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue checkout'),
  );

  return $form;
}

/**
 * Handle review step.
 *
 * @return array
 *   page object
 */
function uc_optile_review() {
  if (!isset($_SESSION['cart_order']) || ($order = uc_order_load($_SESSION['cart_order'])) == FALSE) {
    unset($_SESSION['cart_order']);
    unset($_SESSION['have_details']);
    drupal_set_message(t('An error has occurred in your Optile payment. Please review your cart and try again.'));
    drupal_goto('cart');
  }

  $build['instructions'] = array('#markup' => t("Your order is almost complete!  Please fill in the following details and click 'Continue checkout' to finalize the purchase."));

  $build['form'] = drupal_get_form('uc_optile_payment_form', $order);

  return $build;
}

/**
 * Handles 'returnUrl' for Optile.
 *
 * Marks the order as complete.
 */
function uc_optile_return() {
  if (!empty($_SESSION['cart_order'])) {
    $order = uc_order_load($_SESSION['cart_order']);

    if ($order->payment_method == 'optile') {
      uc_optile_create_optile_order_data($order->order_id);

      unset($_SESSION['uc_checkout'][$order->order_id]['do_review']);
      $_SESSION['uc_checkout'][$order->order_id]['do_complete'] = TRUE;
    }
  }

  drupal_goto('cart/checkout/complete');
}

function uc_optile_cancel() {
  unset($_SESSION['cart_order']);

  drupal_set_message(t('Your order was canceled. Please feel free to continue shopping or contact us for assistance.'));

  drupal_goto('cart');
}

/**
 * Handles the IPN for Optile.
 */
function uc_optile_ipn() {
  $ipn = new UcOptileModelNotification();
  $ipn->processNotification();
}

class UcOptileModelNotification {
  private $ipn_id;

  /**
   * Returns optile notification IPs. All other IPs are blacklisted.
   *
   * Test server (sandbox.oscato.com) => 78.46.61.206
   * Production server (oscato.com) => 213.155.71.162
   *
   * @return string
   *   optile ip
   */
  protected function getAllowedIP() {
    return variable_get('uc_optile_remote_ip');
  }

  protected function getProxyIP() {
    return variable_get('uc_optile_proxy_ip');
  }

  /**
   * Logs msg into watchdog.
   *
   * @param string $message
   *   message to log
   * @param constant $severity
   *   severity level
   */
  protected function log($message, $severity = WATCHDOG_DEBUG) {
    watchdog('uc_optile', $message, array(), $severity);
  }

  /**
   * Processes optile notification.
   *
   * Calls appropriate method based on request type.
   */
  public function processNotification() {

    $params = $this->_params = $_GET;

    $this->log(__METHOD__.' INCOMING IPN');
    $this->log(serialize($params));

    $allowed_ip = $this->getAllowedIP();
    $proxy_ip = $this->getProxyIP();

    if ($proxy_ip && ($_SERVER['REMOTE_ADDR'] != $proxy_ip || !isset($_SERVER['HTTP_X_FORWARDED_FOR']))) {
        // Configured proxy / load balancer IP address does not match.
        $this->log(__METHOD__ .' Bad configuration value for proxy / load balancer', WATCHDOG_ALERT);
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $this->log(__METHOD__ .' Suggested configuration: '. $_SERVER['REMOTE_ADDR'], WATCHDOG_ALERT);
        } else {
            $this->log(__METHOD__ .' No proxy detected!', WATCHDOG_ALERT);
        }
        header($_SERVER['SERVER_PROTOCOL'] .' 403 Forbidden', true, 403);
        die('<h1>Access denied</h1>');
    }
    if ($allowed_ip != ($proxy_ip ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'])) {
        $this->log(__METHOD__ .' UNAUTHORIZED ACCESS FROM: '. $_SERVER['REMOTE_ADDR'], WATCHDOG_ALERT);
        header($_SERVER['SERVER_PROTOCOL'] .' 403 Forbidden', true, 403);
        die('<h1>Access denied</h1>');
    }

    $this->ipn_id = $this->saveIPN($params);

    $order = uc_order_load($params['transactionId']);

    if (!$order) {
      $msg = 'Order with ID ' . $params['transactionId'] . ' not found.';
      $this->exitWithMsg($msg);
    }

    switch ($params['statusCode']) {
      case 'charged':
        $this->handleCharged($order);
        break;
      case 'charged_back':
        $this->handleChargedBack($order);
        break;
      case 'information_requested':
        $this->handleInfoRequested($order);
        break;
      case 'dispute_closed':
        $this->handleDisputeClosed($order);
        break;
//      case 'registered':
      //@TODO: handle 'registered'?
      case 'failed':
      case 'declined':
      case 'aborted':
      case 'canceled':
        $this->handleFailed($order);
        break;
      case 'expired':
        $this->handleExpired($order);
        break;
      case 'pending':
        $this->handlePending($order);
        break;
      default:
        $msg = "Not handling " . $params['statusCode'];
        $this->log($msg, WATCHDOG_NOTICE);
        die($msg);
    }

    die('OK');
  }

  /**
   * Process optile status notification with statusCode 'charged'.
   */
  protected function handleCharged($order) {

    // Match amount.
    $order_sum = (float) $order->order_total;
    $param_sum = (float) $this->_params['amount'];
    $epsilon = 0.00001;
    $param_currency = $this->_params['currency'];
    if (abs($order_sum - $param_sum) > $epsilon) {
      $msg = ' Authorized amount (' . $param_sum . ') does not match order amount (' . $order_sum . ')';

      uc_payment_enter($order->order_id, 'optile', $param_sum, $order->uid, NULL);
      uc_order_comment_save($order->order_id, 0, t('Payment notification amount (@param_sum) does not match order amount (@order_sum)', array(
              '@param_sum' => $param_sum, '@order_sum' => $order_sum)), 'admin');

      $this->exitWithMsg($msg);
    }

    $allowed_states = array('post_checkout');
    if(!in_array(uc_order_status_data($order->order_status, 'state'), $allowed_states)) {
//      uc_order_update_status($order->order_id, 'optile_unexpected_notification');

      // Prepare response to Optile.
      $msg = sprintf(' Current order state does not allow payments (%s / %s)', uc_order_status_data($order->order_status, 'state'), $order->order_status);

      // Prepare order status history comment.
      $comment = 'Rejected payment notification because order status is: ';
      $comment .= $order->order_status;

//      // We won't set unexpected notification for cases where Optile is too
//      // fast with sending IPN's and customer hasn't been redirected to
//      // returnUrl yet where the order will move to post_checkout
      if(uc_order_status_data($order->order_status, 'state') == 'in_checkout') {
        uc_order_comment_save($order->order_id, 0, $comment, 'admin');
        $this->exitWithMsg($msg, true);
      } else {
        $this->exitUnexpectedNotification($order, $msg, $comment);
      }
    }

    // Update payment.
    $this->log(__METHOD__ . ' Updating payment...');

    uc_payment_enter($order->order_id, 'optile', $param_sum, $order->uid, NULL);
    uc_cart_complete_sale($order);
    uc_order_comment_save($order->order_id, 0, t('Optile IPN reported a payment of @amount @currency.', array(
      '@amount' => uc_currency_format($param_sum, FALSE),
      '@currency' => $param_currency,
            )), 'admin');

    $this->log(__METHOD__ .' OK');
    $this->log(__METHOD__ .' Updating Optile quote data..');

    uc_optile_create_optile_order_data($order->order_id);

    list($merchantCode, $merchantToken, $apiUrl) = uc_optile_get_keys();
    $viewUrl = rtrim($apiUrl, '/');
    $viewUrl .= '/monitor/transactions/%s';
    $monitorLink = sprintf($viewUrl, $this->_params['longId']);

    db_update('uc_optile_order_data')
    ->fields(array(
      'long_id' => $this->_params['longId'],
      'payment_network' => $this->_params['network'],
      'monitor_link' => $monitorLink,
    ))
    ->condition('order_id', $order->order_id)
    ->execute();

    $this->log(__METHOD__ . ' DONE');
  }

  /**
   * Process optile status notification.
   *
   * statusCode: 'failed', 'declined', 'aborted', 'canceled'.
   */
  protected function handleFailed($order) {
    $params = $this->_params;

    // Only handle failures regarding payments.
    if ($params['entity'] != 'payment') {
      $msg = ' Not handling ' . $params['entity'] . ' status: "' . $params['statusCode'] . '"';
      $this->log(__METHOD__ . $msg, WATCHDOG_NOTICE);
      die('NOTICE:' . $msg);
    }

    $optileOrderData = uc_optile_find_optile_order_data($order->order_id);

    if (!$optileOrderData) {
        // LongId not set on quote yet. It's not possible to determine the
        // relevance of this "expired" IPN at this moment. Try again later.
        $msg = 'Long ID could not be verified yet; try again later.';

        $this->exitWithMsg($msg, true);
    }
    if($optileOrderData['long_id'] != $params['longId']) {
      // LongId is of a different LIST request. In this context that
      // should be rather strange.
      $msg = ' Long ID could not be matched! Found: '. $optileOrderData['long_id'];
      $this->exitWithMsg($msg);
    }

    if(uc_order_status_data($order->order_status, 'state') == 'canceled') {
      $this->log(__METHOD__ .' Order already canceled.', WATCHDOG_NOTICE);
      return;
    } else if(uc_order_status_data($order->order_status, 'state') != 'post_checkout') {
      // MVR_NAT_SC2 5.3 Unexpected notification, accept and change status.
      $msg = ' Order cannot be canceled! Current status: '. $order->order_status;

      $comment = 'Received notification "'. $params['statusCode'] .'" but order cannot be canceled!';
      $comment .= ' Details: '. $params['resultInfo'];

      $this->exitUnexpectedNotification($order, $msg, $comment);
    }

    // Update order status.
    switch ($params['statusCode']) {
      case 'failed':
        $comment = 'Operation failed due to technical reasons';
        break;

      case 'declined':
        $comment = 'Operation declined by institution';
        break;

      case 'aborted':
        $comment = 'Operation aborted by customer';
        break;

      case 'canceled':
        $comment = 'Operation canceled';
        break;
    }

    if ($params['resultInfo']) {
      $comment .= ' (' . $params['resultInfo'] . ')';
    }

    $this->log(__METHOD__ . ' Updating order status...');

    uc_order_comment_save($order->order_id, 0, $comment, 'admin');
    uc_order_update_status($order->order_id, 'canceled');

    $this->log(__METHOD__ . ' DONE');
  }

  /**
   * Process optile status notification with statusCode 'expired'.
   */
  protected function handleExpired($order) {
    $params = $this->_params;

    // "expired" notifications should reference entity types "session" or
    // "payment".
    if (!in_array($params['entity'], array('session', 'payment'))) {
      $msg = ' Not handling ' . $params['entity'] . ' status: "' . $params['statusCode'] . '"';
      $this->log(__METHOD__ . $msg, WATCHDOG_NOTICE);
      die('NOTICE:' . $msg);
    }

    $optileOrderData = uc_optile_find_optile_order_data($order->order_id);

    if (!$optileOrderData) {
        // LongId not set on quote yet. It's not possible to determine the
        // relevance of this "expired" IPN at this moment. Try again later.
        $msg = 'Long ID could not be verified yet; try again later.';

        $this->exitWithMsg($msg);
    }
    if($optileOrderData['long_id'] != $params['longId']) {
      // LongId is of a different LIST request. This is safe to ignore.
      $msg = ' Ignoring this time out notification.';
      $this->log(__METHOD__ . $msg, WATCHDOG_NOTICE);

      return;
    }

    if(uc_order_status_data($order->order_status, 'state') == 'canceled') {
      $this->log(__METHOD__ .' Order already canceled.', WATCHDOG_NOTICE);
      return;
    } else if(uc_order_status_data($order->order_status, 'state') != 'post_checkout') {
      // MVR_NAT_SC2 5.3 Unexpected notification, accept and change status.
      $msg = ' Order cannot be canceled! Current status: '. $order->order_status;

      $comment = 'Received notification "'. $params['statusCode'] .'" but order cannot be canceled!';
      $comment .= ' Details: '. $params['resultInfo'];
      $this->exitUnexpectedNotification($order, $msg, $comment);
    }

    $comment = 'Payment session timed out';

    if ($params['resultInfo']) {
      $comment .= ' (' . $params['resultInfo'] . ')';
    }

    $this->log(__METHOD__ . ' Updating order status...');

    uc_order_comment_save($order->order_id, 0, $comment, 'admin');
    uc_order_update_status($order->order_id, 'canceled');

    $this->log(__METHOD__ . ' DONE');
  }

  /**
   * Process optile status notification with statusCode 'charged_back'.
   */
  protected function handleChargedBack($order) {
    $params = $this->_params;
    $this->log(__METHOD__ . ' Registering incoming chargeback notification...');

    $comment = 'Registered notification about refunded amount of'. $params['amount'];

    $this->log(__METHOD__ . ' Updating order status...');

    uc_order_comment_save($order->order_id, 0, $comment, 'admin');

    $this->log(__METHOD__ . ' DONE');

    // On top of this in-context notification, notify the customer more intrusively.
    uc_optile_add_merchant_notification(
              'A Chargeback notification has been received from Optile. Please go to Order '. $order->order_id .' and review it.',
              'To review the order that has been charged back, go to Store -> View orders, find the order with ID '. $order->order_id .' and open it.'
              );
  }

  /**
   * Process optile status notification.
   *
   * statusCode: 'pending' - pending payments (eg 3D Secure).
   */
  protected function handlePending($order) {
    $params = $this->_params;

    if (!in_array($params['entity'], array('payment'))) {
      $msg = ' Not handling ' . $params['entity'] . ' status: "' . $params['statusCode'] . '"';
      $this->log(__METHOD__ . $msg, WATCHDOG_NOTICE);
      die('NOTICE:' . $msg);
    }

    // Match amount.
    $order_sum = (float) $order->order_total;
    if(isset($this->_params['amount'])) {
      $param_sum = (float) $this->_params['amount'];
      $epsilon = 0.00001;
      $param_currency = $this->_params['currency'];
      if (abs($order_sum - $param_sum) > $epsilon) {
        $msg = ' Authorized amount (' . $param_sum . ') does not match order amount (' . $order_sum . ')';

        uc_payment_enter($order->order_id, 'optile', $param_sum, $order->uid, NULL);
        uc_order_comment_save($order->order_id, 0, t('Payment notification amount (@param_sum) does not match order amount (@order_sum)', array(
                '@param_sum' => $param_sum, '@order_sum' => $order_sum)), 'admin');

        $this->exitWithMsg($msg);
      }
    }

    if(uc_order_status_data($order->order_status, 'state') != 'post_checkout' && uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
      // MVR_NAT_SC2 5.3 Unexpected notification, accept and change status.
      $comment = 'Received notification "'. $params['statusCode'] .'" but the order is already paid.';
      $comment .= ' Details: '. $params['resultInfo'];

    } else {
      $optileOrderData = uc_optile_find_optile_order_data($order->order_id);
      if($optileOrderData['long_id']) {
        $comment = 'Payment is pending, status will be updated later';
      } else {
        $comment = 'Payment is pending. Updating longId to '.$params['longId'];
      }
      uc_order_comment_save($order->order_id, 0, $comment, 'admin');
    }

    list($merchantCode, $merchantToken, $apiUrl) = uc_optile_get_keys();
    $viewUrl = rtrim($apiUrl, '/');
    $viewUrl .= '/monitor/transactions/%s';
    $monitorLink = sprintf($viewUrl, $this->_params['longId']);

    db_update('uc_optile_order_data')
    ->fields(array(
      'long_id' => $this->_params['longId'],
      'payment_network' => $this->_params['network'],
      'monitor_link' => $monitorLink,
    ))
    ->condition('order_id', $order->order_id)
    ->execute();

    $this->log(__METHOD__ . ' DONE');
  }

  /**
   * Process optile status notification with statusCode 'information_requested'.
   */
  protected function handleInfoRequested($order) {
    $params = $this->_params;
    $this->log(__METHOD__ . ' Registering incoming information_requested notification...');

    $comment = 'Charge disputed by customer; provide more information.';

    $this->log(__METHOD__ . ' Updating order status...');

    uc_order_comment_save($order->order_id, 0, $comment, 'admin');

    $this->log(__METHOD__ . ' DONE');

    // On top of this in-context notification, notify the customer more intrusively.
    uc_optile_add_merchant_notification(
              'A dispute notification has been received from Optile. Please go to Order '. $order->order_id .' and review it.',
              'Information has been requested about a charge. To review the order in question, go to Store -> View orders, find the order with ID '. $order->order_id .' and open it.'
              );
  }

  /**
   * Process optile status notification with statusCode 'dispute_closed'.
   */
  protected function handleDisputeClosed($order) {
    $params = $this->_params;
    $this->log(__METHOD__ . ' Registering incoming dispute_closed notification...');

    $comment = 'Dispute closed.';

    if($params['reasonCode'] == 'chargeback_canceled') {
      $comment .= ' Chargeback canceled.';
    }

    $this->log(__METHOD__ . ' Updating order status...');

    uc_order_comment_save($order->order_id, 0, $comment, 'admin');

    $this->log(__METHOD__ . ' DONE');

    // On top of this in-context notification, notify the customer more intrusively.
    uc_optile_add_merchant_notification(
              'The charge dispute on Order '. $order->order_id .' has been closed.',
              'No further action is required.'
              );
  }

  /**
   * Saves all the IPN data into the db.
   *
   * @param type $params
   */
  private function saveIPN($params) {
    $id = db_insert('uc_optile_notifications')
      ->fields(array(
        'transaction_id' => $params['transactionId'],
        'date' => time(),
        'received_data' => json_encode($params),
        'long_id' => $params['longId'],
        'return_code' => $params['returnCode'],
        'interaction_code' => $params['interactionCode'],
        'reason_code' => $params['reasonCode'],
        'result_info' => $params['resultInfo'],
        'status' => 200,
      ))
      ->execute();

    return $id;
  }

  /**
   * Handles unexpected notification by adding a critical message to the
   * notification inbox, adding a comment to the order history, logging a
   * message to the log file, and giving the log message as a response to
   * Optile. Other than that this will accept the message normally.
   * Calling this method ends code execution.
   *
   * @param Mage_Sales_Model_Order $order
   * @param string $responseMsg
   * @param string $adminMsg
   */
  private function exitUnexpectedNotification($order, $responseMsg, $adminMsg) {

      // Change status to "Unexpected Optile notification" and add message.
//        $order->addStatusToHistory(self::STATUS_UNEXPECTED, $adminMsg)->save();
    uc_order_comment_save($order->order_id, 0, $adminMsg, 'admin');

    // Additionally raise a system-wide alert.
    // Encourage merchant to report bugs.
    $email = 'support@optile.zendesk.com';
    $subject = urlencode('Optile Magento extension: unexpected notification');
    $body = urlencode('Dear supportdesk,

The following unexpected notification was encountered in my Magento store:

statusCode: '. $this->_params['statusCode']. '
longId: '. $this->_params['longId']. '

Order state and status: '. uc_order_status_data($order->order_status, 'state') .' / '. $order->order_status .'
My merchant code is: '. variable_get('uc_optile_merchant_code') .'
');
    $adminMsg .= "<br/>Please report this to Optile: <a href='mailto:$email?subject=$subject&body=$body'>$email</a>";

    uc_optile_add_merchant_notification(
            'Optile Payment Extension: unexpected notification received for order #'. $order->order_id,
            $adminMsg
            );

    // Respond with an error and exit.
    $this->exitWithMsg($responseMsg);
  }

  /**
   * Send response to Optile and exit. Also logs response in the log file.
   *
   * @param string $responseMsg Message to be logged and sent to Optile
   * @param bool $internalError When true, sends HTTP 500, making Optile resend the message later
   */
  private function exitWithMsg($responseMsg, $internalError = false) {
      // Log our response to Optile in the log file.
      $this->log($responseMsg, WATCHDOG_ERROR);

      // Notify Optile of the error.
      if ($internalError) {
          db_update('uc_optile_notifications')
            ->fields(array(
              'status' => 500,
            ))
            ->condition('id', $this->ipn_id)
            ->execute();

          // Response code 500; effectively this makes Optile resend the message later.
          header($_SERVER['SERVER_PROTOCOL'] .' 500 Internal Error', true, 500);
      }
      die("ERROR: $responseMsg");
  }

}
