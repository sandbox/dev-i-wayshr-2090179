<?php
/**
 * Copyright optile GmbH 2013
 * Licensed under the Software License Agreement in effect between optile and
 * Licensee/user (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * http://www.optile.de/software-license-agreement; in addition, a countersigned
 * copy has been provided to you for your records. Unless required by applicable
 * law or agreed to in writing or otherwise stipulated in the License, software
 * distributed under the License is distributed on an "as is” basis without
 * warranties or conditions of any kind, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * @author      i-Ways <dev@i-ways.hr>
 * @copyright   Copyright (c) 2013 optile GmbH. (http://www.optile.de)
 * @license     http://www.optile.de/software-license-agreement
 */

namespace Optile\Request;

class RequestFactory {

    /**
     * @param string $apiUrl URL (domain) to use as base for request URL.
     * @return \Optile\Request\ListRequest
     */
    public static function getListRequest($apiUrl) {
        $url = rtrim($apiUrl, '/');
        return new ListRequest($url);
    }

    /**
     * @param string $url URL to request.
     * @return \Optile\Request\ReloadListRequest
     */
    public static function getReloadListRequest($url) {
        return new ReloadListRequest($url);
    }

    /**
     * @param string $apiUrl URL (domain) to use as base for request URL.
     * @return \Optile\Request\ChargeRequest
     */
    public static function getChargeRequest($apiUrl) {
        $url = rtrim($apiUrl, '/');
        return new ChargeRequest($url);
    }

    /**
     * @param string $url
     * @return \Optile\Request\SimpleRequest
     */
    public static function getSimpleRequest($url) {
        return new SimpleRequest($url);
    }

    /**
     * @param string $component
     * @param string $subcomponent
     * @return \Optile\Request\class
     */
    public static function getComponent($component, $subcomponent='') {
        $class = '\Optile\Request\\'.ucfirst($component) . ucfirst($subcomponent);

        return new $class();
    }

    /**
     * @param string $name
     * @param mixed $message
     * @param int $code [optional]
     * @param \Exception $previous [optional]
     * @return \Exception
     */
    public static function getException($name, $message, $code=null, $previous=null) {
        $class = '\Optile\Request\\'.ucfirst($name) .'Exception';

        return new $class($message, $code, $previous);
    }

}
