<?php
/**
 * Copyright optile GmbH 2013
 * Licensed under the Software License Agreement in effect between optile and
 * Licensee/user (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * http://www.optile.de/software-license-agreement; in addition, a countersigned
 * copy has been provided to you for your records. Unless required by applicable
 * law or agreed to in writing or otherwise stipulated in the License, software
 * distributed under the License is distributed on an "as is” basis without
 * warranties or conditions of any kind, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * @author      i-Ways <dev@i-ways.hr>
 * @copyright   Copyright (c) 2013 optile GmbH. (http://www.optile.de)
 * @license     http://www.optile.de/software-license-agreement
 */

namespace Optile\Request;

abstract class Request extends Component {

    const METHOD_POST = 'post';
    const METHOD_GET = 'get';

    protected $url;
    protected $urlSuffix = '';
    protected $merchantCode;
    protected $merchantToken;

    /**
     * @param string $url
     */
    public function __construct($url) {
        $this->url = (string) $url;
    }

    public function setMerchantCode($merchantCode){
        $this->merchantCode = $merchantCode;
        return $this;
    }

    public function setMerchantToken($merchantToken){
        $this->merchantToken = $merchantToken;
        return $this;
    }

    // @TODO: this should probably not be part of the generic Request object.
    public function setUrl($value) {
        $this->url = rtrim($value, '/');
        return $this;
    }

    /**
     * Send request to Optile and return response.
     * @param string $method post|get
     * @return string
     * @throws @static.mtd:OptileRequestFactory.getException
     */
    public function send($method) {
        if (!isset($this->url) || !strlen($this->url)) {
            $e = RequestFactory::getException('url', 'URL must be set');
            throw $e;
        }

        $url = rtrim($this->url, '/') . $this->urlSuffix;
        $data = $this->getValidatedData();
        $message = $this->serialize($data, $method);

        $curlOpts = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
        );

        if ($method == self::METHOD_POST) {
            $curlOpts += array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $message,
                CURLOPT_HEADER => 1,
                CURLOPT_VERBOSE => 0,
                CURLOPT_HTTPHEADER => array(
                    'Content-length:'. strlen($message),
                    'Accept:application/vnd.optile.payment.simple-v1+json',
                    'Content-type:application/vnd.optile.payment.advanced-v1-extensible+json',
                ),
            );
        } else {
            $url .= $message;
        }

        if(isset($this->merchantCode, $this->merchantToken)) {
            $curlOpts[CURLOPT_USERPWD] = $this->merchantCode.'/'.$this->merchantToken;
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $curlOpts);

        $response = (string) curl_exec($ch);

        curl_close($ch);

        $parts = explode("\r\n", $response);
        $result = array_pop($parts);

        return $result;
    }

    protected function serialize(array $data, $method) {
        switch ($method) {
            case self::METHOD_GET:
                return $this->serializeForGet($data);

            case self::METHOD_POST:
                return $this->serializeForPost($data);
        }
    }

    protected function serializeForPost(array $data) {
        return json_encode($data, JSON_NUMERIC_CHECK);
    }

    protected function serializeForGet(array $data) {
        if (empty($data))
            return '';

        $kvPairs = array();
        foreach ($data as $key => $value) {
            $kvPairs[] = $key .'='. urlencode($value);
        }

        $query = '?'. implode('&', $kvPairs);

        return $query;
    }

}
